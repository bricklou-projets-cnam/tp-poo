namespace TP1;

public class Weapon
{
    public EWeaponType DamageType { get; }
    public int MaxDamage { get; }
    public int MinDamage { get; }

    public string Name { get; }

    public Weapon(string name, int minDamage, int maxDamage, EWeaponType damageType)
    {
        Name = name;
        MinDamage = minDamage;
        MaxDamage = maxDamage;
        DamageType = damageType;
    }

    public enum EWeaponType
    {
        Direct,
        Explosive,
        Guided
    }
}