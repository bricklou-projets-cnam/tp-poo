namespace TP1;

public class SpaceInvaders
{
    private readonly List<Player> _players = new();
    private Armory _armory = new();

    public SpaceInvaders()
    {
        Init();

        Console.WriteLine("Joueurs: {0}", string.Join(", ", _players.Select(p => p.ToString())));

        _armory.ViewWeapons();

        var ss = new SpaceShip(5, 20);

        _players.First().AssignSpaceShip(ss);

        _players.First().Ship?.ViewShip();
    }

    public static void Main()
    {
        var si = new SpaceInvaders();
    }

    private void Init()
    {
        _players.Add(new Player("riri", "Duck", "riri"));
        _players.Add(new Player("fifi", "Duck", "fifi"));
        _players.Add(new Player("loulou", "Duck", "loulou"));
    }
}