namespace TP1;

public class Armory
{
    public List<Weapon> Weapons { get; private set; }

    public Armory()
    {
        Weapons = new List<Weapon>();
        Init();
    }

    private void Init()
    {
        Weapons.Add(new Weapon("Kaboom", 10, 100, Weapon.EWeaponType.Explosive));
        Weapons.Add(new Weapon("Lasor", 2, 20, Weapon.EWeaponType.Direct));
        Weapons.Add(new Weapon("Missile", 1, 5, Weapon.EWeaponType.Guided));
    }

    public void Store(Weapon w)
    {
        Weapons.Add(w);
    }

    public void Remove(Weapon w)
    {
        Weapons.Remove(w);
    }

    public override string ToString()
    {
        return "Armory(" + string.Join(", ", Weapons.Select(w => w.ToString())) + ")";
    }

    public void ViewWeapons()
    {
        Console.WriteLine("Armory Weapons: " + ToString());
    }
}