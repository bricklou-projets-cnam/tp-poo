namespace TP1;

public class Player
{
    private readonly string _alias;
    private readonly string _firstName;
    private readonly string _lastName;

    public string Name => $"{_firstName} {_lastName}";

    public SpaceShip? Ship { get; private set; }

    public Player(string firstName, string lastName, string alias)
    {
        (_firstName, _lastName) = FormatNames(firstName, lastName);
        _alias = alias;
    }

    public void AssignSpaceShip(SpaceShip? ship)
    {
        Ship = ship;
    }

    private static (string, string) FormatNames(string firstName, string lastName)
    {
        var last = lastName.ToUpper();
        var first = char.ToUpper(firstName[0]) + firstName.Substring(1);

        return (first, last);
    }

    public override string ToString()
    {
        return $"{_alias} ({_firstName} {_lastName})";
    }

    public override bool Equals(object? obj)
    {
        if (obj is Player p2) return _alias.Equals(p2._alias);

        return false;
    }
}