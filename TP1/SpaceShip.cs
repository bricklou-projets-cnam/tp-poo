namespace TP1;

public class SpaceShip
{
    private int _currentShield;
    private int _currentStructure;

    private readonly int _maxShield;
    private readonly int _maxStructure;

    public List<Weapon> Weapons { get; private set; }

    public SpaceShip(int maxShield, int maxStructure)
    {
        Weapons = new List<Weapon>();
        _currentShield = _maxShield = maxShield;
        _currentStructure = _maxStructure = maxStructure;
    }

    public int CurrentShield
    {
        get => _currentShield;
        set => _currentShield = Math.Clamp(value, 0, _maxShield);
    }

    public int CurrentStructure
    {
        get => _currentStructure;
        set => _currentStructure = Math.Clamp(value, 0, _maxStructure);
    }

    public bool IsDestroyed => _currentStructure == 0;

    public void AddWeapon(Weapon weapon)
    {
        if (Weapons.Count <= 3)
        {
            if (!Weapons.Contains(weapon)) Weapons.Add(weapon);
        }
        else
        {
            throw new IndexOutOfRangeException("The ship can only equip 3 weapons");
        }
    }

    public void RemoveWeapon(Weapon oWeapon)
    {
        Weapons.Remove(oWeapon);
    }

    public void ClearWeapons()
    {
        Weapons.Clear();
    }

    public void ViewWeapons()
    {
        Console.WriteLine("Weapons: " + string.Join(", ", Weapons.Select(w => w.ToString())));
    }

    public double AverageDamages()
    {
        return Weapons.Select(w => (w.MaxDamage - w.MinDamage) / 2d).Sum() / Weapons.Count;
    }

    public void ViewShip()
    {
        Console.WriteLine("SpaceShip:");
        Console.WriteLine("\tWeapons: {0}", Weapons.Count);
        Console.WriteLine("\tStructure: {0}/{1}", CurrentStructure, _maxStructure);
        Console.WriteLine("\tShield: {0}/{1}", CurrentShield, _maxShield);
    }
}