﻿using Extends;
using Models;
using SpaceInvadersArmory;
using Spaceships;

namespace ConsoleGame;

public class SpaceInvaders
{
    #region Patern singleton

    //implémentation thread safe du patern singleton
    private static readonly Lazy<SpaceInvaders> lazy = new(() => new SpaceInvaders());

    public static SpaceInvaders Instance => lazy.Value;

    private SpaceInvaders()
    {
        Init();
    }

    #endregion Patern singleton

    public List<Player> Players { get; } = new();
    public List<Spaceship> Enemies { get; } = new();

    public List<Spaceship> AllSpaceships = new();


    private void Init()
    {
        Players.Add(new Player("MaXiMe", "haRlé", "Per6fleur"));
        Players.Add(new Player("Guillaume", "urban", "LaGrandeRoutière"));
        Players.Add(new Player("kintali asish-kumar", "PRusty", "DxC"));

        Enemies.Add(new F18());
        Enemies.Add(new Tardis());
        Enemies.Add(new BWing());
        Enemies.Add(new Dart());
        Enemies.Add(new Rocinante());
        Enemies.Add(new ViperMKII());

        foreach (var p in Players) AllSpaceships.Add(p.BattleShip);

        foreach (var spaceship in Enemies) AllSpaceships.Add(spaceship);
    }

    private static void Main(string[] args)
    {
        if (args.Length > 0)
        {
            var importer = new ArmeImporter(args[0], 3);
            importer.GenerateWeapons();
        }

        Armory.ViewArmory();
        foreach (var item in Instance.Players)
        {
            Console.WriteLine(item.ToString());
            item.BattleShip.ViewShip();
        }

        Console.ReadKey();
        
        Console.Write("Best weapons: ");
        foreach (var w in Armory.Instance.BestAverageWeapons)
        {
            Console.Write($"{w.Name}, ");
        }
        Console.WriteLine();

        // Menu();
        Instance.Play();
    }

    private static void Menu()
    {
        var navigation = new NavigationMenu();
        navigation.Add("Jouer", Instance.Play);
        navigation.Add("Quitter", () =>
        {
            Environment.Exit(0);
        });

        navigation.Run();
    }

    private void Play()
    {
        while (Players.Any(p => !p.Lost)) PlayRound();
    }

    private void PlayRound()
    {
        var rnd = new Random();
        List<Spaceship> ordreJeu = new(AllSpaceships.Count);
        var enemiesAliveCount = Enemies.Count(e => !e.IsDestroyed);

        // Génération de l'ordre de jeu
        foreach (var p in Players.Where(s => !s.Lost))
            for (var i = 0; i < enemiesAliveCount; i++)
                if (rnd.ChanceOver(enemiesAliveCount, i))
                {
                    ordreJeu[i] = p.BattleShip;
                    break;
                }

        var enemyIndex = 0;
        for (var i = 0; i < ordreJeu.Count; i++)
            if (!ordreJeu[i].BelongsPlayer)
            {
                ordreJeu[i] = Enemies[enemyIndex];
                enemyIndex++;
            }

        // Chaque début de tour les vaisseaux ayant perdu des points de bouclier en regagne maximum 2.
        Console.WriteLine("Tout les ennemies récupèrent 2 points de bouclier");
        foreach (var enemy in Enemies) enemy.RepairShield(2);

        // Chaque vaisseaux ayant des abilités les utilisent
        foreach (var enemy in Enemies)
        {
            if (enemy is not IAbility enemyWithAbility) continue;

            Console.WriteLine($"{enemy.Name} utilise sont abilité");
            enemyWithAbility.UseAbility(AllSpaceships);
        }

        foreach (var ship in ordreJeu)
        {
            if (ship.IsDestroyed) continue;

            if (ship.BelongsPlayer)
            {
                var rndIndex = rnd.Next(Enemies.Count);
                var ennemy = Enemies[rndIndex];

                Console.WriteLine($"{ship.Name} tire sur le vaisseau de ${ennemy.Name}");
                ship.ShootTarget(ennemy);

                if (ennemy.IsDestroyed)
                    Console.WriteLine($"{ship.Name} a détruit le vaisseau ennemie ${ennemy.Name}");
                ennemy.ShowStats();
            }
            else
            {
                var rndIndex = rnd.Next(Players.Count);
                var player = Players[rndIndex];

                Console.WriteLine($"{ship.Name} tire sur le vaisseau de ${player.Name}");
                ship.ShootTarget(player.BattleShip);

                if (player.Lost)
                    Console.WriteLine($"{ship.Name} a détruit le vaisseau ennemie ${player.Name}");
                player.BattleShip.ShowStats();
            }
        }

        // Le joueur tire sur un vaisseau non détruit au hasard dans la liste d’ennemis.
        // Il aura [1/nombre d’ennemis en vie] chances de tirer en premier [2/nombre d’ennemis en vie] chances de tirer en deuxième et ainsi de suite.  
        /*rnd = new Random();
        var count = Enemies.Count(s => !s.IsDestroyed);
        var enemiesAlive = Enemies.Where(s => !s.IsDestroyed).ToList();
        var randomShip = rnd.Next(count);

        for (var i = 0; i < count; i++)
        {
            if (!rnd.ChanceOver(count, i)) continue;

            Console.WriteLine($"{player.Name} tire sur le vaisseau ennemie ${enemiesAlive[randomShip].Name}");
            player.BattleShip.ShootTarget(enemiesAlive[randomShip]);

            if (enemiesAlive[randomShip].IsDestroyed)
                Console.WriteLine($"{player.Name} a détruit le vaisseau ennemie ${enemiesAlive[randomShip].Name}");

            enemiesAlive[randomShip].ShowStats();
    
            break;
        }*/
    }
}