namespace ConsoleGame;

public class NavigationMenu
{
    private Dictionary<string, Action> _items = new();

    public NavigationMenu()
    {
    }

    public void Add(string name, Action action)
    {
        _items[name] = action;
    }

    public void Add(string name, NavigationMenu menu)
    {
        _items[name] = menu.Run;
    }

    public void Run()
    {
        ShowMenu(_items.Keys.ToList());

        string? input;
        int choice;
        var retry = false;
        do
        {
            if (retry)
            {
                Console.SetCursorPosition(0, Console.CursorTop -1);
                for (var i = 0; i < Console.WindowWidth-1; i++)
                {
                    Console.Write(" ");
                }

                Console.CursorLeft = 0;
            }
            Console.Write("Choix: ");
            input = Console.ReadLine();

            retry = true;
        } while (!int.TryParse(input, out choice) || choice < 0 || choice >= _items.Keys.Count);

        var key = _items.Keys.ElementAt(choice);
        _items[key]();
    }

    private static void ShowMenu(IReadOnlyList<string> items)
    {
        for (var i = 0; i < items.Count(); i++) Console.WriteLine($"{i}. {items[i]}");
    }
}