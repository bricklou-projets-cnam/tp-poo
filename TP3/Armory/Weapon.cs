﻿using System;
using Extends;
using Models;

namespace SpaceInvadersArmory;

public class Weapon : IWeapon
{
    public Guid Id { get; } = new();

    /// <summary>
    /// Le shéma utilisé pour créer l'arme
    /// </summary>
    public WeaponBlueprint Blueprint { get; }

    public string Name { get; set; }
    public EWeaponType Type { get; set; }
    public double MinDamage { get; set; }
    public double MaxDamage { get; set; }
    public double AverageDamage => (MinDamage + MaxDamage) / 2;
    public double ReloadTime { get; set; }
    public double TimeBeforReload { get; set; }

    /// <summary>
    /// Constructeur avec une visibilité internal pour que seule l'armurerie puisse créer des armes.
    /// Par ce moyen on s'assure que toutes les armes proviennent l'armurerie
    /// </summary>
    /// <remarks>Exemple d'utilisation de la visibilité internal</remarks>
    internal Weapon(WeaponBlueprint blueprint)
    {
        Blueprint = blueprint;
        Name = blueprint.Name;
        Type = blueprint.Type;
        MinDamage = blueprint.MinDamage;
        MaxDamage = blueprint.MaxDamage;
        ReloadTime = blueprint.ReloadTime;
        TimeBeforReload = blueprint.ReloadTime;
    }

    public bool IsReload => TimeBeforReload <= 0;

    public double Shoot()
    {
        TimeBeforReload--;

        if (!IsReload)
            return 0;

        TimeBeforReload = ReloadTime;

        var rand = new Random();
        var degat = rand.NextDouble() * (MaxDamage - MinDamage) + MinDamage;

        bool failed;
        switch (Type)
        {
            case EWeaponType.Direct:
                failed = rand.ChanceOver(10);
                if (failed) degat = 0;
                break;
            case EWeaponType.Explosive:
                degat *= 2;
                TimeBeforReload *= 2;

                failed = rand.ChanceOver(4);
                if (failed) degat = 0;
                break;
            case EWeaponType.Guided:
                degat = MinDamage;
                break;
        }

        return degat;
    }

    public override string ToString()
    {
        return Name + " : " + Type + " (" + MinDamage + "-" + MaxDamage + ")";
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as Weapon);
    }

    public bool Equals(Weapon other)
    {
        return other != null &&
               Id.Equals(other.Id);
    }

    public override int GetHashCode()
    {
        return 2108858624 + Id.GetHashCode();
    }

    /// <summary>
    /// Compare les arme en fonction de leur shéma
    /// </summary>
    /// <param name="obj">l'objet à comparer</param>
    /// <returns>le resultat de la comparaison</returns>
    public bool EqualsBlueprint(object obj)
    {
        return Equals(obj as Weapon);
    }

    public bool EqualsBlueprint(Weapon other)
    {
        return other != null &&
               Blueprint.Equals(other.Blueprint);
    }
}