namespace SpaceInvadersArmory;

public class ArmeImporter
{
    private Dictionary<string, int> _wordsCount = new();

    public ArmeImporter(string filename, int minLength, ICollection<string>? blacklist = null)
    {
        if (!File.Exists(filename)) return;

        try
        {
            var text = File.ReadAllText(filename);

            string[] delimiters = {" ", "\n"};
            foreach (var w in text.Split(delimiters, StringSplitOptions.None))
            {
                var parsedWord = string.Join("", w.ToLower().Where(c => !char.IsPunctuation(c)).ToArray());
                if (parsedWord.Length < minLength) continue;
                if (blacklist != null && blacklist.Contains(parsedWord)) continue;

                if (_wordsCount.ContainsKey(parsedWord))
                    _wordsCount[parsedWord]++;
                else
                    _wordsCount[parsedWord] = 0;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Failed to import weapons");
        }
    }

    public void GenerateWeapons()
    {
        foreach (var (key, count) in _wordsCount)
        {
            var length = key.Length;
            var rnd = new Random();
            var type = rnd.Next(2) switch
            {
                0 => EWeaponType.Direct,
                1 => EWeaponType.Explosive,
                2 => EWeaponType.Guided,
                _ => EWeaponType.Direct
            };

            Armory.CreatBlueprint(key, type, length, length+count);
        }
    }
}