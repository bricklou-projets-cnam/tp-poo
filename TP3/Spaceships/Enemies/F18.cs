namespace Spaceships;

using Models;

public class F18 : Spaceship, IAbility
{
    public F18() : base("F18", 15, 0)
    {
    }

    public override void ShootTarget(Spaceship target)
    {
    }

    public bool UseAbility(List<Spaceship> spaceships)
    {
        var spaceshipsAlive = spaceships.Where(w => w.CurrentStructure > 0).ToList();

        var index = spaceshipsAlive.IndexOf(this);

        if (index < 0) return false;

        if (index > 0 && spaceshipsAlive[index - 1].BelongsPlayer)
        {
            spaceshipsAlive[index - 1].TakeDamages(20);
            Structure = 0;
        }
        else if (index < spaceshipsAlive.Count - 1 && spaceshipsAlive[index + 1].BelongsPlayer)
        {
            spaceshipsAlive[index + 1].TakeDamages(20);
            Structure = 0;
        }

        return true;
    }
}