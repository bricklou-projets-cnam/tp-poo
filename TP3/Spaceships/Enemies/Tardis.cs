namespace Spaceships;

using Models;

public class Tardis : Spaceship, IAbility
{
    public Tardis() : base("Tardis", 1, 0)
    {
    }

    public override void ShootTarget(Spaceship target)
    {
    }

    public bool UseAbility(List<Spaceship> spaceships)
    {
        var rnd = new Random();

        var posIn = rnd.Next(spaceships.Count);
        var posOut = rnd.Next(spaceships.Count);

        (spaceships[posOut], spaceships[posIn]) = (spaceships[posIn], spaceships[posOut]);

        return true;
    }
}