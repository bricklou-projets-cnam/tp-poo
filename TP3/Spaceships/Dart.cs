using SpaceInvadersArmory;

namespace Spaceships;

using Models;

public class Dart : Spaceship
{
    public Dart() : base("Dart", 10, 3)
    {
        try
        {
            AddWeapon(
                Armory.CreatWeapon(
                    Armory.Blueprints.First(w => w.Name == "Laser")
                )
            );
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public override void ShootTarget(Spaceship target)
    {
        var weapon = Weapons.First();
        if (weapon.Type == EWeaponType.Direct) weapon.TimeBeforReload = 0;

        var damage = weapon.Shoot();
        target.TakeDamages(damage);
    }
}