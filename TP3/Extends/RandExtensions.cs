namespace Extends;

public static class RandExtensions
{
    public static bool ChanceOver(this Random rand, int max, int chances = 1)
    {
        return rand.Next(1, max) % max < chances;
    }
}