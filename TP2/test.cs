namespace ConsoleGame;

public interface IVoiture
{
    void Accelerer(double force);
    void Freiner(double force);
    void Demarrer();
    void Arretter();
}

public interface IElectrique
{
    void Recharger(int quantite);
}

public interface IEssence
{
    void FaireLePlein(int quantite);
}

public class VoitureElectrique : IVoiture, IElectrique
{
    public void Accelerer(double force)
    {
        throw new NotImplementedException();
    }

    public void Freiner(double force)
    {
        throw new NotImplementedException();
    }

    public void Demarrer()
    {
        throw new NotImplementedException();
    }

    public void Arretter()
    {
        throw new NotImplementedException();
    }


    private int energieDansBatterie = 0;

    public void Recharger(int quantite)
    {
        energieDansBatterie += quantite;
    }
}

public class VoitureEssence : IVoiture, IEssence
{
    public void Accelerer(double force)
    {
        throw new NotImplementedException();
    }

    public void Freiner(double force)
    {
        throw new NotImplementedException();
    }

    public void Demarrer()
    {
        throw new NotImplementedException();
    }

    public void Arretter()
    {
        throw new NotImplementedException();
    }

    private int essenceDansReservoir = 0;

    public void FaireLePlein(int quantite)
    {
        essenceDansReservoir += quantite;
    }
}