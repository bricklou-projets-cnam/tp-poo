﻿using SpaceInvadersArmory;

namespace Models;

public abstract class Spaceship : IEquatable<Spaceship>, ISpaceship
{
    public Guid Id { get; } = new();
    public string Name { get; set; }
    public double Structure { get; set; }
    public double Shield { get; set; }
    public double CurrentStructure { get; set; }
    public double CurrentShield { get; set; }
    public bool BelongsPlayer => owner is not null;

    private Player? owner = null;

    public bool IsDestroyed => CurrentStructure <= 0;
    public int MaxWeapons => 3;
    public List<Weapon> Weapons { get; } = new();
    public double AverageDamages => (Weapons.Sum(x => x.MinDamage) + Weapons.Sum(x => x.MaxDamage)) / 2;

    protected Spaceship(string name, double structure, double shield)
    {
        Name = name;
        Shield = shield;
        Structure = structure;
        CurrentShield = Shield;
        CurrentStructure = Structure;
    }

    public void AddWeapon(Weapon weapon)
    {
        // test si l'arme provien bien de l'armurerie mais c'est quasiment impossible avec les visibilités utilisées
        if (!Armory.IsWeaponFromArmory(weapon)) throw new ArmoryException();
        // évite de dépasser le nombre maximum d'arme sur le vaisseau
        if (Weapons.Count < MaxWeapons)
            Weapons.Add(weapon);
        else
            throw new Exception(Name + " : Max Weapons on ship");
    }

    public void RemoveWeapon(Weapon oWeapon)
    {
        if (Weapons.Contains(oWeapon)) Weapons.Remove(oWeapon);
    }

    public void ClearWeapons()
    {
        Weapons.Clear();
    }

    public void ViewShip()
    {
        Console.WriteLine("===== INFOS VAISSEAU =====");
        Console.WriteLine("Nom : " + Name);
        Console.WriteLine("Points de vie : " + Structure);
        Console.WriteLine("Bouclier : " + Shield);
        ViewWeapons();
        Console.WriteLine("Dommages moyens: " + AverageDamages);
        Console.WriteLine();
    }

    public void ViewWeapons()
    {
        foreach (var item in Weapons) Console.WriteLine(item.ToString());
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as Spaceship);
    }

    public bool Equals(Spaceship other)
    {
        return other != null &&
               Id.Equals(other.Id);
    }

    public override int GetHashCode()
    {
        return 2108858624 + Id.GetHashCode();
    }


    public virtual void TakeDamages(double damages)
    {
        if (CurrentShield >= 0)
        {
            var min = Math.Min(CurrentShield, damages);

            CurrentShield -= min;
            damages -= min;
        }

        if (damages > 0)
        {
            CurrentStructure = Math.Max(CurrentStructure - damages, 0);
            Console.WriteLine($"{Name} as pris {damages} dégâts");
        }
        else
        {
            Console.WriteLine($"{Name} n'as pris aucun dégâts");
        }
    }

    public void RepairShield(double repair)
    {
        CurrentShield += repair;
    }

    public virtual void ShootTarget(Spaceship target)
    {
        var weapons = Weapons.First();
        var damage = weapons.Shoot();
        target.TakeDamages(damage);
    }

    public void ReloadWeapons()
    {
        throw new NotImplementedException();
    }

    public void SetOwner(Player? player)
    {
        owner = player;
    }

    public void ShowStats()
    {
        Console.WriteLine($"Nom: {Name}");
        if (owner is not null) Console.WriteLine($"Propriétaire: {owner.Name}");
        Console.WriteLine($"Structure: {CurrentStructure}" + (IsDestroyed ? " (Détruit)" : ""));
        Console.WriteLine($"Bouclier: {CurrentShield}");
    }
}