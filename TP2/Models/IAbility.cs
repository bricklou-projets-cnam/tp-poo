namespace Models;

public interface IAbility
{
    bool UseAbility(List<Spaceship> spaceships);
}