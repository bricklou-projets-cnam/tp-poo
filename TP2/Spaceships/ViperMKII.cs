using SpaceInvadersArmory;

namespace Spaceships;

using Models;

public class ViperMKII : Spaceship
{
    public ViperMKII() : base("ViperMKII", 10, 15)
    {
        try
        {
            AddWeapon(
                Armory.CreatWeapon(
                    Armory.Blueprints.First(w => w.Name == "Mitrailleuse")
                )
            );

            AddWeapon(
                Armory.CreatWeapon(
                    Armory.Blueprints.First(w => w.Name == "EMG")
                )
            );

            AddWeapon(
                Armory.CreatWeapon(
                    Armory.Blueprints.First(w => w.Name == "Missile")
                )
            );
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public override void ShootTarget(Spaceship target)
    {
        var weapons = Weapons.Where(w => w.IsReload);

        var rnd = new Random();
        var index = rnd.Next(weapons.Count());
        var weapon = weapons.ToArray()[index];

        var damage = weapon.Shoot();
        target.TakeDamages(damage);
    }
}