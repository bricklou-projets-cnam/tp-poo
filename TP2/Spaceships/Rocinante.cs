using Extends;
using SpaceInvadersArmory;

namespace Spaceships;

using Models;

public class Rocinante : Spaceship
{
    public Rocinante() : base("Rocinante", 3, 5)
    {
        AddWeapon(
            Armory.CreatWeapon(
                Armory.Blueprints.First(w => w.Name == "Torpille")
            )
        );
    }

    public override void TakeDamages(double damages)
    {
        var rand = new Random();
        var failed = rand.ChanceOver(2);

        if (failed) return;

        base.TakeDamages(damages);
    }
}