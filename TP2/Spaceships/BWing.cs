using SpaceInvadersArmory;

namespace Spaceships;

using Models;

public class BWing : Spaceship
{
    public BWing() : base("B-Wings", 30, 0)
    {
        try
        {
            AddWeapon(
                Armory.CreatWeapon(
                    Armory.Blueprints.First(w => w.Name == "Hammer")
                )
            );
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public override void ShootTarget(Spaceship target)
    {
        var weapon = Weapons.First();
        if (weapon.Type == EWeaponType.Explosive) weapon.TimeBeforReload = 0;
        var damage = weapon.Shoot();
        target.TakeDamages(damage);
    }
}