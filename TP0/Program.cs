﻿namespace TP0;
// Note: actual namespace depends on the project name.

internal class Program
{
    private static void Main(string[] args)
    {
        var shouldClose = false;
        do
        {
            Question1();

            Question2();

            var o = Question3();

            Question4(o.annee);

            Question8(o.poids, o.taille);

            Question10();

            Question12(out shouldClose);
        } while (!shouldClose);

        Console.WriteLine("Au revoir");
        Thread.Sleep(3000);
    }

    private static void Question1()
    {
        // Question 1
        Console.WriteLine("Bienvenue sur mon programme, jeune étranger imberbe");
    }

    private static void Question2()
    {
        // Question 2
        Console.Write("Donne moi ton nom: ");
        var nom = Console.ReadLine();
        if (nom is null || nom.Any(char.IsDigit))
        {
            Console.WriteLine("Pas de chiffres !");
            return;
        }

        Console.Write("Donne moi ton prénom: ");
        var prenom = Console.ReadLine();
        if (prenom is null || prenom.Any(char.IsDigit))
        {
            Console.WriteLine("Pas de chiffres !");
            return;
        }

        Console.WriteLine("Bonjour, {0}", FormatNom(prenom, nom));
    }

    private static (int taille, int poids, int annee) Question3()
    {
        // Question 3
        Console.Write("Quel est ta taille ? ");
        int taille;
        while (!int.TryParse(Console.ReadLine(), out taille)) Console.WriteLine("Pas un nombre");

        Console.Write("Quel est ton poids ? ");
        int poids;
        while (!int.TryParse(Console.ReadLine(), out poids)) Console.WriteLine("Pas un nombre");

        Console.Write("En quel année es-tu né ? ");
        int annee;
        while (!int.TryParse(Console.ReadLine(), out annee)) Console.WriteLine("Pas un nombre");

        return (taille, poids, annee);
    }

    private static void Question4(int annee)
    {
        // Question 4
        var age = DateTime.Now.Year - annee;
        if (age < 18) Console.WriteLine("Quoi ?! Tu n'es pas majeur ? Dégage de là !");
    }

    private static void Question8(int poids, int taille)
    {
        // Question 8
        var imc = Imc(poids, taille);
        TraiterImc(imc);
    }

    private static void Question10()
    {
        // Question 10
        var v = 0;
        do
        {
            Console.Write("Combien avez vous de cheveux sur votre tête ? ");
            var input = Console.ReadLine();
            if (input is null || input.All(char.IsDigit))
                continue;

            if (!int.TryParse(input, out v)) Console.WriteLine("Nombre invalide");
        } while (v is not (< 100_000 or > 150_000));
    }

    private static void Question12(out bool shouldClose)
    {
        Console.WriteLine("Que voulez vous faire ?");
        Console.WriteLine("1 - Quitter le programme");
        Console.WriteLine("2 - Recommencer le programme");
        Console.WriteLine("3 - Compter jusqu'à 10");
        Console.WriteLine("4 - Téléphoner à Tata Jackeline");


        var choix = 0;
        while (!int.TryParse(Console.ReadLine(), out choix) || choix is not (<= 4 and >= 1))
        {
            if (choix == -1)
                // Flush line
                Console.Write(new string(' ', Console.BufferWidth));
            else
                choix = -1;

            Console.WriteLine("Veuillez entrer un nombre entre 1 et 4");
        }

        shouldClose = true;
        switch (choix)
        {
            case 1:
                break;
            case 2:
                shouldClose = false;
                break;
            case 3:
                Compter();
                break;
            case 4:
                Console.WriteLine("Cela va être compliqué, je ne suis pas un téléphone !");
                break;
        }
    }

    private static void Compter()
    {
        Console.WriteLine("C'est parti pour compter !");
        for (var i = 1; i < 10; i++)
        {
            Console.WriteLine("{0} !", i);
            Thread.Sleep(1000);
        }
    }

    private static string FormatNom(string prenom, string nom)
    {
        return $"{prenom.ToLower()} {nom.ToUpper()}";
    }

    private static float Imc(int poids, int taille)
    {
        return poids / (float) (taille ^ 2);
    }

    private static void TraiterImc(float imc)
    {
        Console.WriteLine("Ton IMC est: {0:0.0}", imc);

        const string OM = "Obésité morbide !",
            OS = "Obésité sévère !",
            OMOD = "Obésité modérée !",
            SUR = "Vous êtes en surpoids !",
            NOR = "Vous êtes de corpulence normale !",
            MAI = "Vous êtes un peu maigrichon !",
            ANO = "Attention anorexie !";

        // Question 9
        if (imc >= 40)
            Console.WriteLine(OM);
        else if (imc >= 35)
            Console.WriteLine(OS);
        else if (imc >= 30)
            Console.WriteLine(OMOD);
        else if (imc >= 25)
            Console.WriteLine(SUR);
        else if (imc >= 18.5)
            Console.WriteLine(NOR);
        else if (imc >= 16.5)
            Console.WriteLine(MAI);
        else
            Console.WriteLine(ANO);
    }
}